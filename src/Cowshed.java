import java.util.ArrayList;
import java.util.List;


public class Cowshed {
	
	public static void main(String[] args) {
        List<Boolean> cows = new ArrayList<>();
        cows.add(true);
        cows.add(true);
        cows.add(true);
        cows.add(false);
        cows.add(true);
        cows.add(true);
        cows.add(false);
        cows.add(false);
        cows.add(true);
        cows.add(true);
        cows.add(false);
        cows.add(true);
        System.out.println(calculate(cows)); 
    }

        
    public static double pricePerMeter =  1.5;
    public static double pricePerCut = 5.0;
    public static double boxWidth = 2.0;

    public static double calculate(List<Boolean> cows) {
        final double pricePerBox = boxWidth * pricePerMeter;
        final int numAffordableEmpty = (int) (pricePerCut / pricePerBox);
        double result = 0.0;

        int emptyCount = numAffordableEmpty;
        boolean inGroup = false;
        int groupCount = 0;

        for (int i = 0; i < cows.size(); i++) {
            if (inGroup) {
                if (cows.get(i)) {
                    groupCount++;
                    emptyCount = numAffordableEmpty;
                } else if (emptyCount > 0) {
                    groupCount++;
                    emptyCount--;
                } else {
                    //trim the group
                    groupCount = groupCount - numAffordableEmpty;
                    //close group
                    inGroup = false;
                    result += (groupCount * pricePerBox) + pricePerCut;
                    groupCount = 0;
                    emptyCount = numAffordableEmpty;
                }
            } else if (cows.get(i)) {
                groupCount++;
                emptyCount = numAffordableEmpty;
                inGroup = true;
            }
        }

        if (inGroup) {
            //trim group
            groupCount = groupCount - (numAffordableEmpty - emptyCount);
            //close group
            result += (groupCount * pricePerBox) + pricePerCut;
        }

        return result;
    }

}
