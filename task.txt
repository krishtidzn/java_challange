Coding Challenge


Farmer Tom owns a farm in Brandenburg, where he has milk cows. This is his cowshed:

+---+---+---+---+---+---+---+---+---+---+---+---+
| x | x | x |   | x | x |   |   | x | x |   | x |
+---+---+---+---+---+---+---+---+---+---+---+---+

As you can see, Tom's cowshed has 12 boxes that fit one cow each, but only 8 cows. Tom's cows made their individual boxes their home, so the don't want to move to any boxes under any circumstances.

But oh no! Last night there was a terrible thunderstorm that severely damaged farmer Tom's cowshed.

The wood in front of all the cow boxes is broken. That needs to be fixed immediately!

+---+---+---+---+---+---+---+---+---+---+---+---+
| x | x | x |   | x | x |   |   | x | x |   | x |
                                                   <-- all boxes are open on this side

Tom goes to the nearby hardware store to buy planks to fix the cowshed.

•� Each box is 2m wide.
•� One meter of planks costs 1.50 Euro.
•� Every cut costs 5 Euro.
•� There is a very (say infinitely) long plank at the hardware store.

Unfortunately, Tom is really low on money. Can you write a program that tells Tom the minimum amount he has to spend in order to get his cows protected again?


Example:
+---+---+---+---+---+---+---+---+---+---+---+---+
| x | x | x |   | x | x |   |   | x | x |   | x |
+---+---+---+---+---+---+---+---+---+---+---+---+
=> 1 Cut from the long plank, 24m wood => 36 + 5 Euro + 41 Euro


+---+---+---+---+---+---+---+---+---+---+---+---+
| x | x | x |   | x | x |   |   | x | x |   | x |
+---+---+---+   +---+---+       +---+---+   +---+
=> 4 cuts, 16m wood => 20 + 24 Euro = 44 Euro


+---+---+---+---+---+---+---+---+---+---+---+---+
| x | x | x |   | x | x |   |   | x | x |   | x |
+---+---+---+---+---+---+       +---+---+---+---+
=> 2 cuts, 20m wood => 10 + 30 Euro = 40 Euro


Bonus: Make your program work for different cowsheds and different wood and cutting prices too.


INPUT: Boolean array
OUTPUT: Minimum price to pay



Please send your program code and the output for the example situation stated above to us.

Happy Coding!

